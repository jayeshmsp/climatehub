<?php
/**
 * Template Name: Boilers Page
 */
?>
<?php 
get_header();
?>
<main>
	<section class="masthead">
		<div class="main_bannar" style="background-image: url('<?php the_field('boilers_banner_image'); ?>');">
			<div class="container height_100">
				<div class="row height_100">
					<div class="col-sm-12 height_100">
						<div class="main_bannar_content">
							<h1 class="main_title"><?php the_field('boilers_banner_title'); ?></h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="main_section boiler_multiple_top">
		<div class="container">
			<div class="row reverse-inner">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<h2 class="sub_title"><?php the_field('boilers_title'); ?></h2>
					<?php the_field('boilers_short_description'); ?>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="main-box right-top">
						<h2 class="sub_title">What do you need?</h2>
					    <div class="row">
						    <form id="filter-form">	
							    <div class="type col-md-6 col-sm-6 col-xs-6">
									<div class="min_title txt-red">Type</div>
									<ul class="type-filter b-filter">
										<?php
										$terms = get_terms( array(
											'taxonomy' => 'boiler_type',
											'hide_empty' => true,
										) );
										foreach ($terms as $term) {
											?>
											<li>
												<input type="checkbox" id="boiler_<?php echo $term->slug; ?>" name="boiler_type[]" value="<?php echo $term->name; ?>"><label for="boiler_<?php echo $term->slug; ?>"><?php echo $term->name; ?></label>
											</li>
											<?php
										}
										?>
									</ul>
							    </div>
							    <div class="type col-md-6 col-sm-6 col-xs-6">
									<div class="min_title txt-red">Fuel</div>
									<ul class="fuel-filter b-filter">
										<?php
										$terms = get_terms( array(
											'taxonomy' => 'boiler_fuel',
											'hide_empty' => true,
										) );
										foreach ($terms as $term) {
											?>
											<li>
												<input type="checkbox" id="boiler_<?php echo $term->slug; ?>" name="boiler_fuel[]" value="<?php echo $term->name; ?>"><label for="boiler_<?php echo $term->slug; ?>"><?php echo $term->name; ?></label>
											</li>
										<?php } ?>
									</ul>
							   </div>
							</form>
					    </div>
						<p>We're here to help, contact us with any questions:</p>
						<a href="<?php echo get_the_permalink(32); ?>" class="btn_common">Contact us</a>
					</div>
				</div>
			</div>
		</div>
    </section>
  	<section class="boiler__multiple multiple_type">
		<div class="container">
			<div class="multiple_type_inner">
                <div class="multiple_type_block">
			       <div class="row boilers_list">
			       	<?php
			       	$args = array(
			       		'post_type'   => 'our_boilers',
			       		'post_status' => 'publish',
			       		'order' => 'ASC',
			       		'posts_per_page' =>-1
			       	);
			       	$query = new WP_Query( $args );
			       	if ( $query->have_posts() ) :
			       		while ( $query->have_posts() ) :
			       			$query->the_post();
			       			$featured_img_url = get_the_post_thumbnail_url($post->ID, 'array(100,100)');
	
							$boiler_type = get_the_terms( $post->ID, 'boiler_type' );
							if($boiler_type) {
								$boiler_type = join(', ', wp_list_pluck($boiler_type, 'name'));
								$boiler_type = '<li><i class="fa fa-check"></i><span>'.$boiler_type.'</span></li>';
							}

							$fuel_type = get_the_terms( $post->ID, 'boiler_fuel' );
							if($fuel_type) {
								$fuel_type = join(', ', wp_list_pluck($fuel_type, 'name'));
								$fuel_type = '<li><i class="fa fa-check"></i><span>'.$fuel_type.'</span></li>';
							}

			       			echo '<div class="col-md-6 col-sm-12 col-xs-12 service_block">
				       			<div class="service_block_main_inner">
					       			<div class="image_block_left">
						       			<div class="service_block_inner">
						       				<img src="'.$featured_img_url.'" alt="boiler-image">
						       			</div>
					       			</div>    
			       					<div class="block_details_right">
			       						<div class="sub_title_small">'.get_the_title().'</div>
			       						<p>'.get_field('boiler_short_description').'</p>
			       						<ul>
			       							'.$boiler_type.'
			       							'.$fuel_type.'
								       		<li><i class="fa fa-check"></i><span>Platinum</span></li>
								       	</ul>
								       	<a class="btn_red_border" href="'.get_the_permalink().'">Details</a>
									</div>
								</div>
							</div>';
			       		endwhile;
			       		wp_reset_postdata();
			       	endif;
			       	?>
					</div>
				</div>
			</div>
		</div>
    </section>

	<section class="testimonial">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="min_title"><?php the_field('home_testimonial_title',7); ?>
					</div>
					    <div class="testimonial_inner owl-carousel">
						<?php
							$args = array(
								'post_type'   => 'testimonials',
								'post_status' => 'publish',
								'posts_per_page' =>-1
							);
							$query = new WP_Query( $args );
							if ( $query->have_posts() ) :
								while ( $query->have_posts() ) :
									$query->the_post();
									echo '<div class="testimonial_item">
										<p>"'.get_field('testimonial_description').'"</p>
										<span>'.get_the_title().'</span>
									</div>';
								endwhile;
								wp_reset_postdata();
							endif;
						?>
						</div>
				</div>
			</div>
		</div>
	</section>
</main>

<?php get_footer(); ?>
<script type="text/javascript">
jQuery(document).ready(function($){

   	$('#filter-form').trigger('reset');

    $('.b-filter').on('change','input[type=checkbox]', function(){
        $('.boilers_list').html('<div class="load_gif"><img src="<?= get_stylesheet_directory_uri(); ?>/images/ajax-loader.gif" style="margin:0 auto;"/></div>');
        var boiler_type = $(this).val();

        var boiler_type = $(".type-filter input:checkbox:checked").map(function(){
            return $(this).val();
        }).get();
        
        var fuel_type = $(this).val();
        var fuel_type = $(".fuel-filter input:checkbox:checked").map(function(){
            return $(this).val();
        }).get();
        

        jQuery.ajax({
            url: "<?= admin_url('admin-ajax.php'); ?>",
            type: 'post',
            data: {
                'action':'boiler_filter_function',
                'boiler_type':boiler_type,
                'fuel_type':fuel_type
			},
            success:function(data) {
                $('.boilers_list').html(data);
            },
            error: function(error){
                console.log('error');
            }
        });
    })
})
</script>