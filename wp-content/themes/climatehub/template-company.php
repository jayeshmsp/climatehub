<?php
/**
 * Template Name: About Page
 */
?>
<?php get_header(); ?>
<main>
	<section class="masthead">
		<div class="main_bannar" style="background-image: url('<?php the_field('company_banner_Image'); ?>');">
			<div class="container height_100">
				<div class="row height_100">
					<div class="col-sm-12 height_100">
						<div class="main_bannar_content">
							<h1 class="main_title"><?php the_field('company_banner_title'); ?></h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="main_section">
		<div class="container">
			<div class="row reverse-inner">
				<div class="col-md-6 col-sm-6"> 
					<h2 class="sub_title"><?php the_field('company_sub_title'); ?></h2>
					<p><?php the_field('company_description'); ?></p>
				</div>
				<div class="col-md-6 col-sm-6"> 
					<div class="main-box main-about"><div class="min_title txt-red"><?php the_field('company_about_title'); ?></div>
						<h2 class="sub_title"><?php the_field('company_about_sub_title'); ?></h2>
						<div class="main-box_link">
						<?php
							$company_further_details_links = get_field('company_further_details_links'); 
							if( $company_further_details_links ): 
								foreach( $company_further_details_links as $post): setup_postdata($post);
									echo '<p><a href="'.get_the_permalink().'" class="txt-red-bottom">'.get_the_title().'</a></p>';
								endforeach;
								wp_reset_postdata();
							else:
								echo "No details found.";
							endif;
						?>
						</div>
					</div>
					<div class="main-bottom">
						<div>
							<img src="<?php the_field('company_about_image'); ?>" alt="about-img">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	  
	<section class="related_case">
		<div class="container">
			<div class="related_case_inner">
				<div class="row">
					<div class="col-md-6 col-sm-6"> 
						<h2 class="sub_title">Related services</h2>
						<div class="related_case_link">
						<?php
							$company_related_services = get_field('company_related_services'); 
							if( $company_related_services ): 
								foreach( $company_related_services as $post): setup_postdata($post);
									echo '<p><a href="'.get_the_permalink().'" class="txt-red-bottom">'.get_the_title().'</a></p>';
								endforeach; 
								wp_reset_postdata();
							else:
								echo "No related services found.";
							endif;
						?>
						</div>
					</div>
					<div class="col-md-6 col-sm-6">
						<h2 class="sub_title">Relevant boilers</h2>
						<div class="related_case_link">
						<?php
							$company_relevant_boilers = get_field('company_relevant_boilers'); 
							if( $company_relevant_boilers ): 
								foreach( $company_relevant_boilers as $post): setup_postdata($post);
									echo '<p><a href="'.get_the_permalink().'" class="txt-red-bottom">'.get_the_title().'</a></p>';
								endforeach;
								wp_reset_postdata();
							else:
								echo "No relevant boilers found.";
							endif;
						?>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>
