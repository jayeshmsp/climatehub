<?php get_header(); ?>

<main>
    <section class="masthead">
        <div class="main_bannar boiler__single">
            <div class="container height_100">
                <div class="row height_100">
                    <div class="col-sm-12 height_100 single_boiler_top">
                        <div class="col-md-6 col-sm-6 main_image_top">
                         <?php $featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');?>
                         <img src="<?php echo $featured_img_url; ?>" alt="boiler-top-small">
                     </div>
                     <div class="col-md-6 col-sm-6 main_title_top">
                        <div class="main_bannar_content">
                            <h1 class="main_title"><?php the_field('boiler_banner_title'); ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="main_section boiler_single_top">
    <div class="container">
        <div class="row reverse-inner">
            <div class="col-md-6 col-sm-6 col-xs-12 left_content">
                <h2 class="sub_title"><?php the_title(); ?></h2>
                <?php the_field('boiler_description'); ?>
                <ul>
                    <?php foreach( get_cfc_meta( 'boiler' ) as $key => $value ){ ?>
                        <?php $author = get_cfc_field( 'boiler','list-of-benefits', false, $key ); ?>
                        <li>
                            <i class="fa fa-check"></i><span><?php echo $author; ?></span>
                        </li>
                    <?php }  ?>
                </ul>
                <a href="<?php echo get_the_permalink(32); ?>" class="btn_common">Contact us</a>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 right_content">
                <div class="main-box right-top">
                    <h2 class="sub_title">Quick stats</h2>
                    <div class="row">
                        <div class="type col-md-12 col-sm-12 col-xs-12">
                            <ul>
                            <?php
                                $terms = get_the_terms($post->ID,'boiler_type');
                                if(!empty($terms)) {
                                    echo '<li><i class="fa fa-check-square"></i><span>'.$terms[0]->name.'</span> </li>';
                                }

                                $terms_fuel = get_the_terms($post->ID,'boiler_fuel');
                                if(!empty($terms_fuel)) {
                                    echo '<li><i class="fa fa-check-square"></i><span>'.$terms_fuel[0]->name.'</span> </li>';
                                }
                            ?>
                            <li><i class="fa fa-check-square"></i><span>Platinum</span></li>

                            <?php
                            $price=get_field('boiler_price');
                            if(!empty($price)) {
                                echo '<li><i class="fa fa-check-square"></i><span>'.$price.'</span></li>';
                            }
                            ?>
                                
                           </ul>
                     </div>
                 </div>
                 <p>We're here to help, contact us with any questions:</p>
                 <a href="<?php echo get_the_permalink(32); ?>" class="btn_common">Contact us</a>
             </div>
             <div class="video_boiler">
                <iframe width="560" height="315" src="<?php the_field('boiler_video_link'); ?>"frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>

</section>

<section class="testimonial">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="min_title "><?php the_field('home_testimonial_title',7); ?></div>
                <div class="testimonial_inner owl-carousel">
                    <?php
                    $args = array(
                        'post_type'   => 'testimonials',
                        'post_status' => 'publish',
                        'posts_per_page' =>-1
                    );

                    $query = new WP_Query( $args );
                    if ( $query->have_posts() ) :
                        while ( $query->have_posts() ) :
                            $query->the_post();
                            echo '<div class="testimonial_item">
                            <p>"'.get_field('home_testimonial_description').'"</p>
                            <span>'.get_the_title().'</span>
                            </div>';
                        endwhile;
                        wp_reset_postdata();
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
</main>
<?php get_footer();?>