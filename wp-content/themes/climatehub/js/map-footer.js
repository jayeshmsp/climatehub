var map;
    function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: 51.291229, lng: 0.308030},
			mapTypeControl: false,
			streetViewControl: false,
			disableDefaultUI: true,
			zoom: 15,
			styles: []
        });



        var image = '/dms/wp-content/themes/dms/images/jpg/map-icon-blue.png';
        marker = new google.maps.Marker({
		    map: map,
		    draggable: false,
		    icon: image,
		    opacity: 1,
		    animation: google.maps.Animation.DROP,
		    position: {lat: 51.291229, lng: 0.308030},
		});
		marker.addListener('click', toggleBounce);

    }

    function toggleBounce() {
		if (marker.getAnimation() !== null) {
			marker.setAnimation(null);
		} else {
			marker.setAnimation(google.maps.Animation.BOUNCE);
		}
	}