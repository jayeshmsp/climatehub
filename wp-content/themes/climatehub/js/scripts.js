/*----------*\
  IE Mobile
\*----------*/
if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
    var msViewportStyle = document.createElement('style')
    msViewportStyle.appendChild(
        document.createTextNode(
            '@-ms-viewport{width:auto!important}'
        )
    )
    document.querySelector('head').appendChild(msViewportStyle)
}

/*------------------------------------*\
  SVG FALLBACK FOR OLD BROWSERS
\*------------------------------------*/

function svgasimg() {
    return document.implementation.hasFeature(
        "http://www.w3.org/TR/SVG11/feature#Image", "1.1");
}

if (!svgasimg()) {
    var e = document.getElementsByTagName("img");
    if (!e.length) {
        e = document.getElementsByTagName("IMG");
    }
    for (var i = 0, n = e.length; i < n; i++) {
        var img = e[i],
            src = img.getAttribute("src");
        if (src.match(/svgz?$/)) {
            /* URL ends in svg or svgz */
            img.setAttribute("src",
                img.getAttribute("data-fallback"));
        }
    }
}

var wowNumber = new WOW({
    boxClass: 'wowNumber',
    callback: function(box) {
        $('.wowNumber').each(function() {
            var nmb = $(this).data('number');
            $(this).text(nmb);
        })
    }
});

wowNumber.init();

var wow = new WOW({
    boxClass: 'wow'
});

wow.init();

$(function() {

    $('.navbar-toggle').click(function() {
        $('html').toggleClass("show-menu");
    });

/*------------------------------------*\
  SLIDER
\*------------------------------------*/


    $(".masthead__overlay__slider").owlCarousel({
        margin: 30,
        nav: true,
        dots: false,
        items: 1
    });

    $(".owl-prev").html('<i class="fa fa-chevron-left"></i>');
    $(".owl-next").html('<i class="fa fa-chevron-right"></i>');

    $('.landlords-testimonials').owlCarousel({
        loop: true,
        margin: 20,
        nav: false,
        dots: true,
        items: 1,
        autoplay: true,
        autoplayTimeout: 5000
    });

});