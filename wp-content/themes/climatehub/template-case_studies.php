<?php
/**
 * Template Name: Case_studies Page
 */
?>
<?php get_header(); ?>
<main>
	<section class="masthead">
		<div class="main_bannar" style="background-image: url('<?php the_field('case_studies_banner_image'); ?>');">
			<div class="container height_100">
				<div class="row height_100">
					<div class="col-sm-12 height_100">
						<div class="main_bannar_content">
							<h1 class="main_title"><?php the_field('case_studies_banner_title'); ?></h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="main_section multiple_top">
		<div class="container">
			<div class="row reverse-inner">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<h2 class="sub_title"><?php the_field('case_studies_title'); ?></h2>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="main-box service-main">
						<div class="min_title txt-red"><?php the_field('case_studies_about_title');?>
							
						</div>
						<h2 class="sub_title"><?php the_field('case_studies_about_sub_title'); ?></h2>
						<?php the_field('case_studies_description'); ?>
						<a href="<?php echo get_the_permalink(32); ?>" class="btn_common">Contact us</a>
					</div>
				</div>
			</div>
		</div>
    </section>
    
    <section class="multiple_type">
		<div class="container">
            <div class="multiple_type_inner">
			    <div class="multiple_type_block">
                    <div class="row">
						<?php
							$args = array(
								'post_type'   => 'case_studies',
								'post_status' => 'publish',
								'order' => 'ASC',
								'posts_per_page' =>-1
							);
							$query = new WP_Query( $args );
							if ( $query->have_posts() ) :
								while ( $query->have_posts() ) :
									$query->the_post();
									$featured_img_url = get_the_post_thumbnail_url($post->ID, 'full'); 

									echo '<div class="col-md-4 col-sm-6 case-study">
										<div class="case_studies_block">
											<a href="'.get_the_permalink().'">
												<div class="case_studies_img">
													<img src="'.$featured_img_url.'" alt="case-study-image">
												</div>
												<div class="case_studies_block_details">
													<div class="sub_title_small">'.get_the_title().'</div>
													<div>'.get_field('case_study_short_description').'</div>
												</div>
											</a>
										</div>
									</div>';
								endwhile;
								wp_reset_postdata();
							else:
								echo "<div class='container'>No case studies found.</div>";
							endif;
						?>
				    </div>
				</div>
			</div>
		</div>
    </section>

	<section class="testimonial">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="min_title"><?php the_field('home_testimonial_title',7); ?></div>
					<div class="testimonial_inner owl-carousel">
						<?php
						$args = array(
							'post_type'   => 'testimonials',
							'post_status' => 'publish',
							'posts_per_page' =>-1
						);
						$query = new WP_Query( $args );
						if ( $query->have_posts() ) :
							while ( $query->have_posts() ) :
								$query->the_post();
								echo '<div class="testimonial_item">
								<p>"'.get_field('testimonial_description').'"</p>
								<span>'.get_the_title().'</span>
								</div>';
							endwhile;
							wp_reset_postdata();
						endif;
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>