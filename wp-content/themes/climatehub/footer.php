<footer class="footer">
    <div class="inner__footer">
        <div class="container">
            <?php dynamic_sidebar('footer-chat'); ?>
            <div class="row">
                <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
                    <?php dynamic_sidebar('footer-top'); ?>
                </div> 
                <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
                    <div class="footer__bottom">
                        <?php dynamic_sidebar('footer-section-1'); ?>
                        <?php dynamic_sidebar('footer-section-2'); ?>
                        <?php dynamic_sidebar('footer-section-3'); ?>
                        <?php dynamic_sidebar('footer-section-4'); ?>
                        <?php dynamic_sidebar('footer-section-5'); ?>
                    </div>
                </div>
            </div> 
        </div> 
    </div>
</footer>

<?php wp_footer(); ?>

<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script>

</body>
</html>