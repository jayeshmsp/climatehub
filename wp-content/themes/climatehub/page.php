<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 */

get_header(); ?>
<?php
// Start the loop.
while ( have_posts() ) : the_post();
?>
<div id="wrapperin2">
	<div class="wrapper">
		<div class="content">
			<div class="conttp">
				<?php //get_sidebar(); ?>
				<div class="rightmains">
					<div class="rightmain">
						<div class="rightinn_ban">
							<big class="rightinn_big"><?php the_title(); ?></big>
							<?php if(get_field('display_featured_image')=='Yes'){ ?>
							<!-- <br class="clear"> -->
								<div>
									<div id="ref_pic_1"><span></span><?php the_post_thumbnail('full'); ?></div><br>
								</div> 
								<?php
								}
								the_content();									
								?>
								
						</div>
						
					</div>
					<?php if(get_field('image_1') ){ ?>
					<div class="lights">
						<div class="lightin">
							<?php
								$image_1 = find_thumbnail_img(get_field('image_1'));
							?>
							 <div class="lightimg"><a rel="jack" href="<?php echo get_field('image_1'); ?>"><img alt="" src="<?php echo $image_1; ?>"></a></div>
						</div>
						<?php if(get_field('image_2') ){ ?>
							<div class="lightin">
								<?php
									$image_1 = find_thumbnail_img(get_field('image_2'));
								?>
								 <div class="lightimg"><a rel="jack" href="<?php echo get_field('image_2'); ?>"><img alt="" src="<?php echo $image_1; ?>"></a></div>
							</div>
						<?php } ?>
						<?php if(get_field('image_3') ){ ?>
							<div class="lightin">
								<?php
									$image_1 = find_thumbnail_img(get_field('image_3'));
								?>
								 <div class="lightimg"><a rel="jack" href="<?php echo get_field('image_3'); ?>"><img alt="" src="<?php echo $image_1; ?>"></a></div>
							</div>
						<?php } ?>
						<?php if(get_field('image_4') ){ ?>
							<div class="lightin">
								<?php
									$image_1 = find_thumbnail_img(get_field('image_4'));
								?>
								 <div class="lightimg"><a rel="jack" href="<?php echo get_field('image_4'); ?>"><img alt="" src="<?php echo $image_1; ?>"></a></div>
							</div>
						<?php } ?>
						<?php if(get_field('image_5') ){ ?>
							<div class="lightin">
								<?php
									$image_1 = find_thumbnail_img(get_field('image_5'));
								?>
								 <div class="lightimg"><a rel="jack" href="<?php echo get_field('image_5'); ?>"><img alt="" src="<?php echo $image_1; ?>"></a></div>
							</div>
						<?php } ?>
						<?php if(get_field('image_6') ){ ?>
							<div class="lightin">
								<?php
									$image_1 = find_thumbnail_img(get_field('image_6'));
								?>
								 <div class="lightimg"><a rel="jack" href="<?php echo get_field('image_6'); ?>"><img alt="" src="<?php echo $image_1; ?>"></a></div>
							</div>
						<?php } ?>
						<?php if(get_field('image_7') ){ ?>
							<div class="lightin">
								<?php
									$image_1 = find_thumbnail_img(get_field('image_7'));
								?>
								 <div class="lightimg"><a rel="jack" href="<?php echo get_field('image_7'); ?>"><img alt="" src="<?php echo $image_1; ?>"></a></div>
							</div>
						<?php } ?>
						<?php if(get_field('image_8') ){ ?>
							<div class="lightin">
								<?php
									$image_1 = find_thumbnail_img(get_field('image_8'));
								?>
								 <div class="lightimg"><a rel="jack" href="<?php echo get_field('image_8'); ?>"><img alt="" src="<?php echo $image_1; ?>"></a></div>
							</div>
						<?php } ?>
						<?php if(get_field('image_9') ){ ?>
							<div class="lightin">
								<?php
									$image_1 = find_thumbnail_img(get_field('image_9'));
								?>
								 <div class="lightimg"><a rel="jack" href="<?php echo get_field('image_9'); ?>"><img alt="" src="<?php echo $image_1; ?>"></a></div>
							</div>
						<?php } ?>
						<?php if(get_field('image_10') ){ ?>
							<div class="lightin">
								<?php
									$image_1 = find_thumbnail_img(get_field('image_10'));
								?>
								 <div class="lightimg"><a rel="jack" href="<?php echo get_field('image_10'); ?>"><img alt="" src="<?php echo $image_1; ?>"></a></div>
							</div>
						<?php } ?>	
					</div>						
					<?php } ?>					
					<br class="clear">
					<?php if(get_field('view_contact_information')){ ?>
					<a class="btn_contact thickbox" href="#TB_inline?width=300&amp;height=160&amp;inlineId=id_contact"></a>
					<div id="id_contact">                                	
						<?php echo get_field('view_contact_information'); ?>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endwhile; ?>
<?php get_footer(); ?>	