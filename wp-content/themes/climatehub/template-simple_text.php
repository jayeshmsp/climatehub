<?php
/**
 * Template Name: Simple Text Page
 */
get_header(); ?>

<main>
	<section class="masthead">
		<div class="main_bannar" style="background-image: url('<?php the_field('banner_image'); ?>');">
			<div class="container height_100">
				<div class="row height_100">
					<div class="col-sm-12 height_100">
						<div class="main_bannar_content">
							<h1 class="main_title"><?php the_field('banner_text'); ?></h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="main_section terms_main">
		<div class="container">
			<div class="row reverse-inner">
				<div class="col-md-12 col-sm-12">
					<h2 class="sub_title"><?php the_title(); ?></h2>
					<?php the_field('description'); ?>
				</div>
				
			</div>
		</div>
	</section>
</main>

<?php get_footer(); ?>