<?php get_header();?>
<main>
	<section class="masthead">
			<div class="main_bannar" style="background-image: url('<?php the_field('service_banner_image'); ?>');">
			<div class="container height_100">
				<div class="row height_100">
					<div class="col-sm-12 height_100">
						<div class="main_bannar_content">
							<h1 class="main_title"><?php the_title(); ?></h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="main_section">
		<div class="container">
			<div class="row reverse-inner">
				<div class="col-md- col-sm-6"> 
					<h2 class="sub_title"><?php the_field('service_sub_title'); ?></h2>
					<?php the_field('service_description'); ?>
				</div>
				<div class="col-md-6 col-sm-6"> 
					<div class="main-box">
						<?php echo do_shortcode("[contact-form-7 id='139' title='Contact form 1']"); ?>
					</div>
				</div>
			</div>
		</div>	
    </section>
	  
	  <section class="related_case">
		<div class="container">
			<div class="related_case_inner">
			   <div class="row">
					<div class="col-md-6"> 
						<h2 class="sub_title">Related case studies</h2>
						<div class="related_case_link">

						<?php
							$service_related_case_studies = get_field('service_related_case_studies'); 
							if( $service_related_case_studies ): 
								foreach( $service_related_case_studies as $post): setup_postdata($post);
									echo '<p><a href="'.get_the_permalink().'" class="txt-red-bottom">'.get_the_title().'</a></p>';
								endforeach; 
								wp_reset_postdata();
							else:
								echo "No related case studies found.";
							endif;
						?>
						
						</div>
					</div>
					<div class="col-md-6">
						<h2 class="sub_title">Relevant boilers</h2>
						<div class="related_case_link">
						<?php
							$service_relevant_boilers = get_field('service_relevant_boilers'); 
							if( $service_relevant_boilers ): 
								foreach( $service_relevant_boilers as $post): setup_postdata($post);
									echo '<p><a href="'.get_the_permalink().'" class="txt-red-bottom">'.get_the_title().'</a></p>';
								endforeach;
								wp_reset_postdata();
							else:
								echo "No relevant boilers found.";
							endif;
							?>
						</div>
					</div>
			   </div>	
			</div>
		</div>
	  </section>
	  
      </main>
<?php get_footer(); ?>