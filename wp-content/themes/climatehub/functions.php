<?php

// custom class
add_filter( 'body_class', 'custom_body_class' );
function custom_body_class( $classes ) {
global $post;
$classes[] = $post->post_name;
return $classes;
}

if ( ! isset( $content_width ) ) {
	$content_width = 660;
}

/**
 * The Climate Hub only works in WordPress 4.1 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.1-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'theclimatehub_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * @since The Climate Hub 1.0
 */
function theclimatehub_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/theclimatehub
	 * If you're building a theme based on theclimatehub, use a find and replace
	 * to change 'theclimatehub' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'theclimatehub' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'theclimatehub' ),
		'social'  => __( 'Social Links Menu', 'theclimatehub' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

	/*
	 * Enable support for custom logo.
	 *
	 * @since The Climate Hub 1.5
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 248,
		'width'       => 248,
		'flex-height' => true,
	) );

	$color_scheme  = theclimatehub_get_color_scheme();
	$default_color = trim( $color_scheme[0], '#' );

	// Setup the WordPress core custom background feature.

	/**
	 * Filter The Climate Hub custom-header support arguments.
	 *
	 * @since The Climate Hub 1.0
	 *
	 * @param array $args {
	 *     An array of custom-header support arguments.
	 *
	 *     @type string $default-color     		Default color of the header.
	 *     @type string $default-attachment     Default attachment of the header.
	 * }
	 */
	add_theme_support( 'custom-background', apply_filters( 'theclimatehub_custom_background_args', array(
		'default-color'      => $default_color,
		'default-attachment' => 'fixed',
	) ) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css', theclimatehub_fonts_url() ) );

	// Indicate widget sidebars can use selective refresh in the Customizer.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif; // theclimatehub_setup
add_action( 'after_setup_theme', 'theclimatehub_setup' );

/**
 * Register widget area.
 *
 * @since The Climate Hub 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar
 */
function theclimatehub_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'theclimatehub' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'theclimatehub' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'theclimatehub_widgets_init' );

if ( ! function_exists( 'theclimatehub_fonts_url' ) ) :
/**
 * Register Google fonts for The Climate Hub.
 *
 * @since The Climate Hub 1.0
 *
 * @return string Google fonts URL for the theme.
 */
function theclimatehub_fonts_url() {
	$fonts_url = '';
	$fonts     = array();
	$subsets   = 'latin,latin-ext';

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Noto Sans, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Noto Sans font: on or off', 'theclimatehub' ) ) {
		$fonts[] = 'Noto Sans:400italic,700italic,400,700';
	}

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Noto Serif, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Noto Serif font: on or off', 'theclimatehub' ) ) {
		$fonts[] = 'Noto Serif:400italic,700italic,400,700';
	}

	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Inconsolata, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Inconsolata font: on or off', 'theclimatehub' ) ) {
		$fonts[] = 'Inconsolata:400,700';
	}

	/*
	 * Translators: To add an additional character subset specific to your language,
	 * translate this to 'greek', 'cyrillic', 'devanagari' or 'vietnamese'. Do not translate into your own language.
	 */
	$subset = _x( 'no-subset', 'Add new subset (greek, cyrillic, devanagari, vietnamese)', 'theclimatehub' );

	if ( 'cyrillic' == $subset ) {
		$subsets .= ',cyrillic,cyrillic-ext';
	} elseif ( 'greek' == $subset ) {
		$subsets .= ',greek,greek-ext';
	} elseif ( 'devanagari' == $subset ) {
		$subsets .= ',devanagari';
	} elseif ( 'vietnamese' == $subset ) {
		$subsets .= ',vietnamese';
	}

	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) ),
			'subset' => urlencode( $subsets ),
		), 'https://fonts.googleapis.com/css' );
	}

	return $fonts_url;
}
endif;

/**
 * JavaScript Detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since The Climate Hub 1.1
 */
function theclimatehub_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'theclimatehub_javascript_detection', 0 );

/**
 * Enqueue scripts and styles.
 *
 * @since The Climate Hub 1.0
 */
function theclimatehub_scripts() {
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'theclimatehub-fonts', theclimatehub_fonts_url(), array(), null );

	// Add Genericons, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.2' );

	// Load our main stylesheet.
	wp_enqueue_style( 'theclimatehub-style', get_stylesheet_uri() );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'theclimatehub-ie', get_template_directory_uri() . '/css/ie.css', array( 'theclimatehub-style' ), '20141010' );
	wp_style_add_data( 'theclimatehub-ie', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'theclimatehub-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'theclimatehub-style' ), '20141010' );
	wp_style_add_data( 'theclimatehub-ie7', 'conditional', 'lt IE 8' );

	wp_enqueue_script( 'theclimatehub-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20141010', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'theclimatehub-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20141010' );
	}

	wp_enqueue_script( 'theclimatehub-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20150330', true );
	wp_localize_script( 'theclimatehub-script', 'screenReaderText', array(
		'expand'   => '<span class="screen-reader-text">' . __( 'expand child menu', 'theclimatehub' ) . '</span>',
		'collapse' => '<span class="screen-reader-text">' . __( 'collapse child menu', 'theclimatehub' ) . '</span>',
	) );
}
add_action( 'wp_enqueue_scripts', 'theclimatehub_scripts' );

/**
 * Add preconnect for Google Fonts.
 *
 * @since The Climate Hub 1.7
 *
 * @param array   $urls          URLs to print for resource hints.
 * @param string  $relation_type The relation type the URLs are printed.
 * @return array URLs to print for resource hints.
 */
function theclimatehub_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'theclimatehub-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '>=' ) ) {
			$urls[] = array(
				'href' => 'https://fonts.gstatic.com',
				'crossorigin',
			);
		} else {
			$urls[] = 'https://fonts.gstatic.com';
		}
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'theclimatehub_resource_hints', 10, 2 );

/**
 * Add featured image as background image to post navigation elements.
 *
 * @since The Climate Hub 1.0
 *
 * @see wp_add_inline_style()
 */
function theclimatehub_post_nav_background() {
	if ( ! is_single() ) {
		return;
	}

	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );
	$css      = '';

	if ( is_attachment() && 'attachment' == $previous->post_type ) {
		return;
	}

	if ( $previous &&  has_post_thumbnail( $previous->ID ) ) {
		$prevthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $previous->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-previous { background-image: url(' . esc_url( $prevthumb[0] ) . '); }
			.post-navigation .nav-previous .post-title, .post-navigation .nav-previous a:hover .post-title, .post-navigation .nav-previous .meta-nav { color: #fff; }
			.post-navigation .nav-previous a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	if ( $next && has_post_thumbnail( $next->ID ) ) {
		$nextthumb = wp_get_attachment_image_src( get_post_thumbnail_id( $next->ID ), 'post-thumbnail' );
		$css .= '
			.post-navigation .nav-next { background-image: url(' . esc_url( $nextthumb[0] ) . '); border-top: 0; }
			.post-navigation .nav-next .post-title, .post-navigation .nav-next a:hover .post-title, .post-navigation .nav-next .meta-nav { color: #fff; }
			.post-navigation .nav-next a:before { background-color: rgba(0, 0, 0, 0.4); }
		';
	}

	wp_add_inline_style( 'theclimatehub-style', $css );
}
add_action( 'wp_enqueue_scripts', 'theclimatehub_post_nav_background' );

/**
 * Display descriptions in main navigation.
 *
 * @since The Climate Hub 1.0
 *
 * @param string  $item_output The menu item output.
 * @param WP_Post $item        Menu item object.
 * @param int     $depth       Depth of the menu.
 * @param array   $args        wp_nav_menu() arguments.
 * @return string Menu item with possible description.
 */
function theclimatehub_nav_description( $item_output, $item, $depth, $args ) {
	if ( 'primary' == $args->theme_location && $item->description ) {
		$item_output = str_replace( $args->link_after . '</a>', '<div class="menu-item-description">' . $item->description . '</div>' . $args->link_after . '</a>', $item_output );
	}

	return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'theclimatehub_nav_description', 10, 4 );

/**
 * Add a `screen-reader-text` class to the search form's submit button.
 *
 * @since The Climate Hub 1.0
 *
 * @param string $html Search form HTML.
 * @return string Modified search form HTML.
 */
function theclimatehub_search_form_modify( $html ) {
	return str_replace( 'class="search-submit"', 'class="search-submit screen-reader-text"', $html );
}
add_filter( 'get_search_form', 'theclimatehub_search_form_modify' );

/**
 * Modifies tag cloud widget arguments to display all tags in the same font size
 * and use list format for better accessibility.
 *
 * @since The Climate Hub 1.9
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array The filtered arguments for tag cloud widget.
 */

function theclimatehub_widget_tag_cloud_args( $args ) {
	$args['largest']  = 22;
	$args['smallest'] = 8;
	$args['unit']     = 'pt';
	$args['format']   = 'list';

	return $args;
}
add_filter( 'widget_tag_cloud_args', 'theclimatehub_widget_tag_cloud_args' );


/**
 * Implement the Custom Header feature.
 *
 * @since The Climate Hub 1.0
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 *
 * @since The Climate Hub 1.0
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 *
 * @since The Climate Hub 1.0
 */
require get_template_directory() . '/inc/customizer.php';

add_action( 'after_setup_theme', 'remove_default_menu', 11 );
function remove_default_menu(){
	unregister_nav_menu('primary');
	unregister_nav_menu('social');
}

function register_my_menus() {
  	register_nav_menus(
	    array(
			'primary-menu' => __( 'Primary Menu' ),
	    )
  	);
}
add_action( 'init', 'register_my_menus' );

add_action( 'widgets_init', 'theme_slug_widgets_init' );
function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Footer Section 1', 'footer-section-1-widget' ),
        'id' => 'footer-section-1',
        'description' => __( 'Footer Section 1', 'footer-section-1-widget' ),
        'before_widget' => '<div id="footer-section-1" class="footer__contact">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6 class="widgettitle">',
		'after_title'   => '</h6>',
    ) );
    register_sidebar( array(
        'name' => __( 'Footer Section 2', 'footer-section-2-widget' ),
        'id' => 'footer-section-2',
        'description' => __( 'Footer Section 2', 'footer-section-2-widget' ),
        'before_widget' => '<div id="footer-section-2" class="address">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="footer__title">',
		'after_title'   => '</div>',
    ) );

    register_sidebar( array(
        'name' => __( 'Footer Section 3', 'footer-section-3-widget' ),
        'id' => 'footer-section-3',
        'description' => __( 'Footer Section 3', 'footer-section-3-widget' ),
        'before_widget' => '<div id="footer-section-3" class="info">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="footer__title">',
		'after_title'   => '</div>',
    ) );

    register_sidebar( array(
        'name' => __( 'Footer Section 4', 'footer-section-4-widget' ),
        'id' => 'footer-section-4',
        'description' => __( 'Footer Section 4', 'footer-section-4-widget' ),
        'before_widget' => '<div id="footer-section-4" class="opening__hours">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="footer__title">',
		'after_title'   => '</div>',
    ) );

    register_sidebar( array(
        'name' => __( 'Footer Section 5', 'footer-section-5-widget' ),
        'id' => 'footer-section-5',
        'description' => __( 'Footer Section 5', 'footer-section-5-widget' ),
        'before_widget' => '<div class="social__media">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6 class="widgettitle">',
		'after_title'   => '</h6>',
    ) );
   

    register_sidebar( array(
        'name' => __( 'Footer Top', 'Footer-Top-widget' ),
        'id' => 'footer-top',
        'description' => __( 'Footer Top', 'footer-top-widget' ),
        'before_widget' => '<div id="footer-top" class="footer__top">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6 class="widgettitle">',
		'after_title'   => '</h6>',
    ) );

    register_sidebar( array(
        'name' => __( 'Footer Chat', 'Footer-Chat-widget' ),
        'id' => 'footer-chat',
        'description' => __( 'footer chat', 'footer-chat-widget' ),
        'before_widget' => '<div id="footer-chat" class="footer__outer">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6 class="widgettitle">',
		'after_title'   => '</h6>',
    ) );
	
	register_sidebar( array(
        'name' => __( 'Header Contact Info', 'header-top-widget' ),
        'id' => 'header-contact-info',
        'description' => __( 'Header Contact Info', 'header-contact-info-widget' ),
        'before_widget' => '<div class="connet_link">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6 class="widgettitle">',
		'after_title'   => '</h6>',
    ) );
    register_sidebar( array(
        'name' => __( 'Header Social', 'header-social-widget' ),
        'id' => 'header-social',
        'description' => __( 'Header Social', 'header-social-widget' ),
        'before_widget' => '<div class="header_social">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6 class="widgettitle">',
		'after_title'   => '</h6>',
    ) );
    register_sidebar( array(
        'name' => __( 'Header Contact', 'header-contact-widget' ),
        'id' => 'header-contact',
        'description' => __( 'Header Contact', 'header-contact-widget' ),
        'before_widget' => '<div class="contact_link">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6 class="widgettitle">',
		'after_title'   => '</h6>',
    ) );
}


function boiler_filter_function(){
	
	$boiler_type = $_POST['boiler_type'];
	$fuel_type = $_POST['fuel_type'];

	if(!empty($boiler_type) || !empty($fuel_type)) {
		$args = array(
			'post_type' => 'our_boilers',
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'order' => 'ASC',
			'tax_query' => array(
				'relation' => 'OR',
				array(
					'taxonomy' => 'boiler_type',
					'field' => 'name',
					'terms' => $boiler_type
				),
				array(
					'taxonomy' => 'boiler_fuel',
					'field' => 'name',
					'terms' => $fuel_type
				),
			),
		);
	} else {
		$args = array(
			'post_type' => 'our_boilers',
			'post_status' => 'publish',
			'order' => 'ASC',
			'posts_per_page' => -1
		);
	}

	$query = new WP_Query( $args );
	if ( $query->have_posts() ) :
		while ( $query->have_posts() ) :
			$query->the_post();
			$featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');
	
			$boiler_type = get_the_terms( $post->ID, 'boiler_type' );
			if($boiler_type) {
				$boiler_type = join(', ', wp_list_pluck($boiler_type, 'name'));
			}

			$fuel_type = get_the_terms( $post->ID, 'boiler_fuel' );
			if($fuel_type) {
				$fuel_type = join(', ', wp_list_pluck($fuel_type, 'name'));
			}

			echo '<div class="col-md-6 col-sm-12 col-xs-12 service_block">
       			<div class="service_block_main_inner">
	       			<div class="image_block_left">
		       			<div class="service_block_inner">
		       				<img src="'.$featured_img_url.'" alt="boiler-image">
		       			</div>
	       			</div>    
   					<div class="block_details_right">
   						<div class="sub_title_small">'.get_the_title().'</div>
   						<p>Donec nec justo eget felis facilisis fermentum. Aliquam porttito.</p>
   						<ul>
   							<li><i class="fa fa-check"></i><span>'.$boiler_type.'</span></li>
				       		<li><i class="fa fa-check"></i><span>'.$fuel_type.'</span></li>
				       		<li><i class="fa fa-check"></i><span>Platinum</span></li>
				       	</ul>
				       	<a class="btn_red_border" href="'.get_the_permalink().'">Details</a>
					</div>
				</div>
			</div>';

		endwhile;
		wp_reset_postdata();
	endif;
die();
}
add_action("wp_ajax_boiler_filter_function", "boiler_filter_function");
add_action("wp_ajax_nopriv_boiler_filter_function", "boiler_filter_function");