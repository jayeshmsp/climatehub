<?php
/**
 * Template Name: Home Page
 */
?>
<?php get_header(); ?>

<main>

    <section class="masthead">
        <div class="main_bannar" style="background-image: url('<?php the_field('home_banner_image');?>');">
            <div class="container height_100">
                <div class="row height_100">
                    <div class="col-sm-12 height_100">
                        <div class="main_bannar_content">
                            <h1 class="main_title"><?php the_field('home_banner_title');?></h1>
                            <p class="title_bottom"><?php the_field('home_banner_sub_title');?></p>
                            <a href="<?php echo get_field('home_about_our_company_link')['url']; ?>" class="btn_red_pink"><?php echo get_field('home_about_our_company_link')['title']; ?></a>
                            <div class="main_today">
                            <?php 
                                $weekday = date('D');
                                if ($weekday == "Sun") {
                                    echo 'closed today <i class="far fa-clock" aria-label="clock"></i> <span>'.get_field('home_sun').'</span>';
                                } elseif ($weekday == "Sat") {
                                    echo 'open today <i class="far fa-clock" aria-label="clock"></i> <span>'.get_field('home_sat').'</span>';
                                } else {
                                    echo 'open today <i class="far fa-clock" aria-label="clock"></i> <span>'.get_field('home_mon-fri').'</span>';
                                }
                            ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="design-install">
        <div class="container">
            <div class="row">
                <div class="design-install_inner reverse-inner">
                    <div class=""><img src="<?php the_field('home_about_image'); ?>" alt="about-image"></div>
                    <div class="design-install-des main-box ">
                        <div class="min_title txt-red"><?php the_field('home_about_title');?></div>
                        <h2 class="sub_title"><?php the_field('home_about_sub_title');?></h2>
                        <p><?php the_field('home_about_description');?></p>
                        <a href="<?php echo get_field('home_read_more_link')['url']; ?>" class="btn_red_border"><?php echo get_field('home_read_more_link')['title']; ?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="our_services">
        <div class="container">
            <div class="our_services_inner">
                <div class="row">
                    <div class="col-md-12">
                        <div class="min_title"><?php the_field('home_service_title'); ?></div>
                        <h2 class="sub_title"><?php the_field('home_service_sub_title');?></h2>
                        <p><?php the_field('home_service_description');?></p>                           
                    </div>
                </div>
                <div class="row our_services_block_main">
                <?php
                    $our_services = get_field('home_our_services');
                    if( $our_services ): 
                        foreach( $our_services as $post): setup_postdata($post);
                            echo '<div class="col-md-3 col-sm-6">
                                <div class="our_services_inner_block">
                                    <img src='.get_field('service_icon_image').' alt="service-icon">
                                    <h3 class="sub_title_small">'.get_the_title().'</h3>
                                    <p>'.get_field('service_short_description').'</p>
                                    <a class="read_link" href="'.get_the_permalink().' ">Read more</a>
                                </div> 
                            </div>';
                        endforeach; 
                        wp_reset_postdata(); 
                    endif;
                ?>
                </div>
                <div class="our_services_contact">
                    <div class="contact_img"><img src="<?php the_field('home_contact_image'); ?>" alt="contact-image"></div>
                    <div class="contact_details">
                        <div class="min_title"><?php the_field('home_contact_title'); ?></div>
                        <div class="contact_call"><a href="tel:<?php echo preg_replace('/\s+/', '', get_field('home_contact_number')); ?>"><?php the_field('home_contact_number'); ?></a></div>
                        <div class="contact_maill"><a href="mailto:<?php echo preg_replace('/\s+/', '', get_field('home_contact_mail')); ?>"><?php the_field('home_contact_mail'); ?></a></div>
                    </div>
                </div>
            </div>
            <a href="<?php echo get_field('home_service_link')['url']; ?>" class="btn_red_pink"><?php echo get_field('home_service_link')['title'];?></a>
        </div>
    </section>

    <section class="case_studies">
        <div class="container">
            <div class="case_studies_inner">
                <div class="row center-align">
                    <div class="col-md-9 col-sm-8">
                        <div class="min_title txt-red"><?php the_field('home_case_study_title'); ?></div>
                        <h2 class="sub_title"><?php the_field('home_case_study_sub_title'); ?></h2>
                    </div>
                    <div class="col-md-3 text-right col-sm-4">
                        <a href="<?php echo get_field('home_case_study_link')['url']; ?>" class="btn_red_border"><?php echo get_field('home_case_study_link')['title']; ?></a>
                    </div>
                </div>
                <div class="row case_studies_block_main ">
                <?php
                    $case_studies = get_field('home_case_studies'); 
                    if( $case_studies ):
                        foreach( $case_studies as $post): setup_postdata($post);
                            $featured_img_url = get_the_post_thumbnail_url($post->ID);
                            echo '<div class="col-md-4 col-sm-6">
                                <div class="case_studies_block">
                                    <a href="'.get_the_permalink().'">
                                        <div class="case_studies_img"><img src="'.$featured_img_url.'" alt="case-study-image">
                                        </div>
                                        <div class="case_studies_block_details">
                                            <div class="sub_title_small">'.get_the_title().'</div>
                                            <div>'.get_field('case_study_short_description').'</div>
                                        </div>
                                    </a>
                                </div>
                            </div>';
                        endforeach; 
                        wp_reset_postdata(); 
                    endif;
                ?>
                </div>
            </div>
        </div>
    </section>

    <section class="testimonial">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="min_title"><?php the_field('home_testimonial_title'); ?></div>
                    <div class="testimonial_inner owl-carousel">
                    <?php
                        $args = array(
                            'post_type'   => 'testimonials',
                            'post_status' => 'publish',
                            'posts_per_page' =>-1
                        );
                        $query = new WP_Query( $args );
                        if ( $query->have_posts() ) :
                            while ( $query->have_posts() ) :
                                $query->the_post();
                                echo '<div class="testimonial_item">
                                    <p>"'.get_field('testimonial_description').'"</p>
                                    <span>'.get_the_title().'</span>
                                </div>';
                            endwhile;
                            wp_reset_postdata();
                        endif;
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>

<?php get_footer(); ?>