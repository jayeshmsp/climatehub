<?php get_header(); ?>

<main>
    <section class="masthead">
        <div class="main_bannar" style="background-image: url('<?php the_field('article_banner_image'); ?>');">
            <div class="container height_100">
                <div class="row height_100">
                    <div class="col-sm-12 height_100">
                        <div class="main_bannar_content">
                          <h1 class="main_title"><?php echo get_the_title(); ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="main_section">
        <div class="container">
            <div class="row reverse-inner">
                <div class="col-md-6 col-sm-6"> 
                    <h2 class="sub_title"><?php the_field('article_sub_title'); ?></h2>
                       <?php the_field('article_description'); ?>
                </div>
                <div class="col-md-6 col-sm-6"> 
                    <div class="main-box">
                       <?php echo do_shortcode("[contact-form-7 id='139' title='Contact form 1']"); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="related_case">
        <div class="container">
            <div class="related_case_inner">
                <div class="row">
                    <div class="col-md-6 col-sm-6"> 
                       <h2 class="sub_title">Related services </h2>
                       <div class="related_case_link">
                        <?php
                            $article_related_services = get_field('article_related_services'); 
                            if( $article_related_services ): 
                                foreach( $article_related_services as $post): setup_postdata($post);
                                    echo '<p><a href="'.get_the_permalink().'" class="txt-red-bottom">'.get_the_title().'</a></p>';
                                endforeach; 
                                wp_reset_postdata();
                            else:
                                echo "No related services found.";
                            endif;
                        ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <h2 class="sub_title">Relevant boilers </h2>
                        <div class="related_case_link">
                        <?php
                            $article_relevant_boilers = get_field('article_relevant_boilers'); 
                            if( $article_relevant_boilers ): 
                                foreach( $article_relevant_boilers as $post): setup_postdata($post);
                                    echo '<p><a href="'.get_the_permalink().'" class="txt-red-bottom">'.get_the_title().'</a></p>';
                                endforeach;
                                wp_reset_postdata();
                            else:
                                echo "No relevant boilers found.";
                            endif;
                        ?>
                        </div>
                    </div>
                </div>	
            </div>
        </div>
    </section>

</main>

<?php get_footer(); ?>