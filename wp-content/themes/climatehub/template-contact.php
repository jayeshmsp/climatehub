<?php
/**
 * Template Name: Contact Page
 */
get_header(); ?>
<main>
    <section class="masthead">
        <div class="main_bannar" style="background-image: url('<?php the_field('contact_banner_image');?>');">
            <div class="container height_100">
                <div class="row height_100">
                    <div class="col-sm-12 height_100">
                        <div class="main_bannar_content">
                            <h1 class="main_title"><?php echo get_field('contact_banner_title'); ?></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="main_section">
        <div class="container">
            <div class="row reverse-inner">
                <div class="col-md-6 col-sm-6"> 
                    <h2 class="sub_title"><?php the_field('contact_title'); ?></h2>
                    <?php the_field('contact_info'); ?>
                    <div class="contact_link">
                        <p><span class="contact_icon"><i class="fa fa-phone" aria-label="phone"></i></span><span><a href="tel:<?php echo preg_replace('/\s+/', '', get_field('contact_number')); ?>" class="txt-red-bottom"><?php the_field('contact_number'); ?></a></span> </p>
                        <p><span class="contact_icon"><i class="far fa-envelope" aria-label="envelope"></i></span><span><a href="mailto:<?php echo preg_replace('/\s+/', '', get_field('contact_email')); ?>" class="txt-red-bottom"><?php the_field('contact_email'); ?></a></span> </p>
                    </div> 
                    <div class="contact_address">
                        <p><?php the_field('contact_address'); ?></p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6"> 
                    <div class="main-box">
                        <?php echo do_shortcode("[contact-form-7 id='139' title='Contact form 1']"); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="footer-top">
        <div class="container"></div>
    </section>

</main>
<?php get_footer(); ?>