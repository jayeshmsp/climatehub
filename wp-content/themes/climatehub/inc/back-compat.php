<?php
/**
 * The Climate Hub back compat functionality
 *
 * Prevents The Climate Hub from running on WordPress versions prior to 4.1,
 * since this theme is not meant to be backward compatible beyond that and
 * relies on many newer functions and markup changes introduced in 4.1.
 *
 * @package WordPress
 * @subpackage The_Climate_Hub
 * @since The Climate Hub 1.0
 */

/**
 * Prevent switching to The Climate Hub on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since The Climate Hub 1.0
 */
function theclimatehub_switch_theme() {
	switch_theme( WP_DEFAULT_THEME, WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'theclimatehub_upgrade_notice' );
}
add_action( 'after_switch_theme', 'theclimatehub_switch_theme' );

/**
 * Add message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * The Climate Hub on WordPress versions prior to 4.1.
 *
 * @since The Climate Hub 1.0
 */
function theclimatehub_upgrade_notice() {
	$message = sprintf( __( 'The Climate Hub requires at least WordPress version 4.1. You are running version %s. Please upgrade and try again.', 'theclimatehub' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevent the Customizer from being loaded on WordPress versions prior to 4.1.
 *
 * @since The Climate Hub 1.0
 */
function theclimatehub_customize() {
	wp_die( sprintf( __( 'The Climate Hub requires at least WordPress version 4.1. You are running version %s. Please upgrade and try again.', 'theclimatehub' ), $GLOBALS['wp_version'] ), '', array(
		'back_link' => true,
	) );
}
add_action( 'load-customize.php', 'theclimatehub_customize' );

/**
 * Prevent the Theme Preview from being loaded on WordPress versions prior to 4.1.
 *
 * @since The Climate Hub 1.0
 */
function theclimatehub_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( __( 'The Climate Hub requires at least WordPress version 4.1. You are running version %s. Please upgrade and try again.', 'theclimatehub' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'theclimatehub_preview' );
