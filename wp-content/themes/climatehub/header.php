<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon-16x16.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!--<link href="https://fonts.googleapis.com/css?family=Nunito:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=latin-ext,vietnamese" rel="stylesheet"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" />
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <div class="side-wrapper">
        <!-- Header Section -->
        <header class="header">
            <nav class="navbar" role="navigation">
                <div class="container">
                    <div class="navbar-header logo_header">
                        <a class="logo-img" href="<?php echo site_url(); ?>">
                        <?php
                           $custom_logo_id = get_theme_mod( 'custom_logo' );
                           $custom_logo_url = wp_get_attachment_image_url( $custom_logo_id , 'full' );
                           echo '<img src="' . esc_url( $custom_logo_url ) . '" alt="">';
                        ?>
                        </a>
                    </div>

                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".main-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar top-bar"></span>
                        <span class="icon-bar middle-bar"></span>
                        <span class="icon-bar bottom-bar"></span>
                    </button>

                    <div class="collapse navbar-collapse main-collapse navbar-right header-menu-main">
                        <div class="header_top">
                            <?php dynamic_sidebar('header-contact-info'); ?>
                            <?php dynamic_sidebar('header-social'); ?>
                            <?php dynamic_sidebar('header-contact'); ?>
                        </div>
                        <div class="header-menu">
                            <?php wp_nav_menu( array( 
                                'theme_location' => 'primary-menu',
                                'menu_class' => 'nav navbar-nav nav--main navbar-right'
                            ) ); ?>
                        </div>
                    </div>
                </div>
            </nav>
        </header>
        <!-- Header Section -->